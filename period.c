#include "period.h"

// Commands Table (indeed a list)
struct scheduled_command *ct = NULL;
// Size of the list
size_t ctSize = 0;

// FIFO Pipe where messages are read and sent
int fdFifo;

// Flag to know if the program must stop or not
bool keepRunning = true;

// Number of processes launched and not waited
size_t nbAliveProcesses;

int main(int argc, char* argv[]) {
	// Write the daemon's PID in a file
	// Open the file
	FILE *ficPID = fopen("/tmp/period.pid", "r");
	if (ficPID) {
		// PID file already exists, don't run 2 daemons
		exit(1);
	}
	// Unable to open it, so there is no need to close
	// Create the file with write access
	ficPID = fopen("/tmp/period.pid", "w");
	if (!ficPID) {
		exit(2);
	}

	// Writing the number with fprintf
	if (1 > fprintf(ficPID, "%d", getpid())) {
		exit(3);
	}

	// File Descriptors of log files
	int fdOut, fdErr;
	// Standard Output redirection
	fdOut = open("/tmp/period.out", O_WRONLY | O_CREAT | O_TRUNC, 0644);
	if (fdOut == -1) {
		exit(4);
	}
	if (dup2(fdOut, 1) < 0) {
		exit(5);
	}

	// Start message
	time_t timeReady = time(NULL);
	printf("Standard output of periodic opened at %s", ctime(&timeReady));

	// Error Output redirection
	fdErr = open("/tmp/period.err", O_WRONLY | O_CREAT | O_TRUNC, 0644);
	if (fdErr == -1) {
		exit(5);
	}
	if (dup2(fdErr, 2) < 0) {
		exit(6);
	}

	// Start message
	fprintf(stderr, "Error log for periodic started at %s", ctime(&timeReady));

	// Now that we can use perror, close PID file
	if (fclose(ficPID)) {
		perror("fclose");
		exit(7);
	}

	// Pipe FIFO
	struct stat statFile;
	if (stat("/tmp/period.fifo", &statFile)) {
		// Error, probably doesn't exist
		// Create the file
		if (mkfifo("/tmp/period.fifo", 0666)) {
			perror("mkfifo");
			exit(8);
		}
	} else {
		// Exists, check type
		if (!S_ISFIFO(statFile.st_mode)) {
			// Ask removal of the conflicting file
			fprintf(stderr, "Error, please remove the file /tmp/periodic.fifo\n");
			exit(9);
		}
	}

	// Open the pipe
	fdFifo = open("/tmp/period.fifo", O_RDWR);
	if (fdFifo == -1) {
		perror("open fifo");
		exit(10);
	}

	// Check the log files directory
	if (stat("/tmp/period", &statFile)) {
		// Error, probably doesn't exist
		if (mkdir("/tmp/period", 0755)) {
			perror("mkdir");
			exit(11);
		}
	} else {
		// Exists, check type
		if (!S_ISDIR(statFile.st_mode) || ((statFile.st_mode & 0755) != 0755)) {
			// Ask removal
			fprintf(stderr, "Error, please remove the folder /tmp/period or set its rights to 755\n");
			exit(12);
		}
	}

	// Signals catching
	// Program's ending
	struct sigaction siga;
	siga.sa_handler = sigEndDaemon;
	sigemptyset(&siga.sa_mask);
	siga.sa_flags = 0;

	if (sigaction(SIGINT, &siga, NULL) == -1) {
		perror("sigaction SIGINT");
		exit(13);
	}
	if (sigaction(SIGQUIT, &siga, NULL) == -1) {
		perror("sigaction SIGQUIT");
		exit(14);
	}
	if (sigaction(SIGTERM, &siga, NULL) == -1) {
		perror("sigaction SIGTERM");
		exit(15);
	}

	// Signals from periodic
	siga.sa_handler = sigSetCmd;
	if (sigaction(SIGUSR1, &siga, NULL) == 1) {
		perror("sigaction SIGUSR1");
		exit(16);
	}
	siga.sa_handler = sigDumpTable;
	if (sigaction(SIGUSR2, &siga, NULL) == -1) {
		perror("sigaction SIGUSR2");
		exit(17);
	}

	// Make the alarm not stop the program
	siga.sa_handler = sigAlarm;
	if (sigaction(SIGALRM, &siga, NULL) == -1) {
		perror("sigaction SIGALRM");
		exit(18);
	}

	// Kill zombies
	siga.sa_handler = sigWaitChild;
	if (sigaction(SIGCHLD, &siga, NULL) == -1) {
		perror("sigaction SIGCHLD");
		exit(19);
	}

	////////////////
	// Main Loop //
	//////////////

	while (keepRunning) {
		// Find and execute requested commands
		struct scheduled_command *curc = ct;
		time_t now = time(NULL);
		time_t nextTime = 0;
		printf("Scanning table at %lu\n", now);
		while (curc) {
			//printf("Command %s supposed to run at %lu with a period of %lu\n", curc->command[0], curc->start, curc->period);
			// We'll need to remove the executed commands with a period of 0
			int toDelete = -1;
			if (curc->start <= now) {
				// Execute the command
				pid_t newFork = fork();
				if (newFork == -1) {
					perror("fork");
				} else if (newFork == 0) {
					// New child process
					// No standard input
					int fdNull = open("/dev/null", O_RDONLY);
					if (fdNull < 0) {
						perror("open in");
						exit(1);
					}
					dup2(fdNull, 0);
					if (close(fdNull)) {
						perror("close in");
						exit(2);
					}
					// Open the log files in append mode, because a command may have run previously with this index
					int fdLogOut, fdLogErr;
					char *pathLog = malloc(sizeof(char)*21);
					// stdout log
					sprintf(pathLog, "/tmp/period/%d.out", curc->index);
					fdLogOut = open(pathLog, O_WRONLY | O_CREAT | O_APPEND, 0644);
					if (fdLogOut < 0) {
						perror("open out");
						exit(3);
					}
					dup2(fdLogOut, 1);
					if (close(fdLogOut)) {
						perror("close out");
						exit(4);
					}

					// stderr log
					sprintf(pathLog, "/tmp/period/%d.err", curc->index);
					fdLogErr = open(pathLog, O_WRONLY | O_CREAT | O_APPEND, 0644);
					if (fdLogErr < 0) {
						perror("open err");
						exit(5);
					}
					dup2(fdLogErr, 2);
					if (close(fdLogErr)) {
						perror("close err");
						exit(6);
					}
					free(pathLog);
					execvp(curc->command[0], curc->command);
					// Couldn't run the command
					perror("execvp");
					// We don't want this process
					exit(1);
				} else {
					// A new process has been created
					++nbAliveProcesses;
				}

				if (curc->period > 0) {
					// Re-schedule the command
					curc->start += curc->period;
				} else {
					// Tell the program we must remove the command from the table
					toDelete = curc->index;
				}
			}
			// Check if this is one of the next commands
			if (nextTime == 0 || curc->start < nextTime) {
				nextTime = curc->start;
			}
			// Jump to next command
			curc = curc->next;
			if (toDelete > -1) {
				// Delete the command
				ct = ct_remove(ct, toDelete);
			}
		}

		// Wait until the next commands have to be launched
		if (nextTime > now) {
			printf("New alarm in %lu seconds\n", nextTime - now);
			alarm(nextTime - now);
		}

		// Wait for the next signal, alarm or not
		printf("Entering pause mode\n");
		pause();
	}

	//////////
	// End //
	////////

	// Remove temporary files
	if (unlink("/tmp/period.pid")) {
		perror("unlink");
	}

	// Kill remaining zombies
	printf("%zu zombie(s) to remove.\n", nbAliveProcesses);
	fflush(stdout); // Flush because the following may freeze forever
	while (nbAliveProcesses) {
		pause(); // SIGCHILD will eventually be received and decrease nbAliveProcesses
	}

	// Add an end message and close the log files
	time_t timeStopped = time(NULL);
	char *strTimeStopped = ctime(&timeStopped);
	printf("--------------\nDaemon stopped at %s", strTimeStopped);
	fflush(stdout);
	if (close(fdOut)) {
		perror("close");
	}
	fprintf(stderr, "--------------\nDaemon stopped at %s", strTimeStopped);
	fflush(stderr);
	if (close(fdErr)) {
		// Maybe it's still opened
		perror("close");
	}

	// Destroy commands table
	if (ct) {
		ct_destroy(ct);
	}

	return 0;
}

////////////////////////////////
// Commands Table Management //
//////////////////////////////

struct scheduled_command* ct_add(struct scheduled_command *self, time_t start, time_t period, int index, char** command) {
	if (ctSize == MAX_COMMANDS) {
		// Can't add more
		return self;
	}

	if (self == NULL || index < self->index) {
		// It must be inserted here
		struct scheduled_command *new = malloc(sizeof(struct scheduled_command));
		new->start = start;
		new->period = period;
		new->command = command;
		new->index = index;
		new->next = self; // Before the current node
		++ctSize;
		return new; // It is the new next
	}
	// Add it further
	self->next = ct_add(self->next, start, period, index + 1, command);
	return self; // Don't change the next
}

struct scheduled_command* ct_remove(struct scheduled_command *self, size_t index) {
	if (!self) {
		// End of the chain
		return NULL;
	}

	if (index > self->index) {
		// Search further
		self->next = ct_remove(self->next, index);
		return self;
	} else if (index < self->index) {
		// We didn't find it
		return self;
	}

	// This is the command we were searching
	// Remove the command array
	char **curStr = self->command;
	while (*curStr) {
		free(*curStr);
		++curStr;
	}
	free(self->command);
	// Save the link to the next element
	struct scheduled_command *next = self->next;
	free(self);
	--ctSize;
	return next; // The next element takes the place of the removed one
}

void ct_destroy(struct scheduled_command *self) {
	if (self->next) {
		// Destroy the following element(s)
		ct_destroy(self->next);
	}
	// Remove the command array
	char **curStr = self->command;
	while (*curStr) {
		free(*curStr);
		++curStr;
	}
	free(self->command);
	// Remove the node
	free(self);
}

//////////////////////
// SIGNAL HANDLERS //
////////////////////

void sigSetCmd(int signum) {
	printf("SIGUSR1!\n");
	if (!keepRunning) {
		// The program won't run it
		return;
	}

	// Receive the action
	char *msg = recv_string(fdFifo);
	printf("Received SIGUSR1 with action %s\n", msg);
	if (!strcmp(msg, "add")) {
		char *msgTime, *msgPeriod;
		msgTime = recv_string(fdFifo);
		msgPeriod = recv_string(fdFifo);
		size_t cmdTime = atol(msgTime);
		size_t cmdPeriod = atol(msgPeriod);
		free(msgTime);
		free(msgPeriod);
		char **commands = recv_argv(fdFifo);
		ct = ct_add(ct, cmdTime, cmdPeriod, 0, commands);
	} else if (!strcmp(msg, "remove")) {
		free(msg);
		msg = recv_string(fdFifo);
		size_t index = atol(msg);
		ct = ct_remove(ct, index);
	} else {
		fprintf(stderr, "Received unknown command with SIGUSR1\n");
	}
	free(msg);
}

void sigDumpTable(int signum) {
	printf("Requested table dump (SIGUSR2). %zu command(s) scheduled\n", ctSize);

	if (!keepRunning) {
		return;
	}

	// Preparing an argv
	char **table = calloc(ctSize + 1, sizeof(char*));
	struct scheduled_command *curc = ct;
	size_t curi = 0;
	// Add each command to the table
	while (curc) {
		// Find the argument's length
		size_t len = 0;
		char **curarg = curc->command;
		while (*curarg) {
			len += 1 + strlen(*curarg);
			++curarg;
		}
		char linestart[256];
		sprintf(linestart, "%d\t%zu\t%zu\t", curc->index, curc->start, curc->period);
		len += strlen(linestart);
		// Write the command
		// '\0' already included
		table[curi] = calloc(len, sizeof(char));
		strcpy(table[curi], linestart);
		strcat(table[curi], curc->command[0]);
		curarg = curc->command + 1;
		while (*curarg) {
			strcat(table[curi], " ");
			strcat(table[curi], *curarg);
			++curarg;
		}
		printf("%s\n", table[curi]);
		curc = curc->next;
		++curi;
	}
	// End of the table
	table[curi] = NULL;

	// Send the table
	send_argv(fdFifo, table);

	// Free the table
	char **curarg = table;
	while (*curarg) {
		free(*curarg);
		++curarg;
	}
	free(table);
}

void sigEndDaemon(int signum) {
	printf("Daemon stopping\n");
	keepRunning = false;
}

void sigWaitChild(int signum) {
	wait(NULL);
	--nbAliveProcesses;
	printf("Killed a zombie. %zu remaining.\n", nbAliveProcesses);
}

void sigAlarm(int signum) {
	printf("Alarm triggered!\n");
}
