#include "message.h"

int send_string(int fd, char *str) {
	size_t len = strlen(str);
	size_t writtenTot = 0, writtenNew;
	if ((writtenNew = write(fd, &len, sizeof(size_t))) <= 0) {
		perror("write");
		return writtenNew;
	}
	while (writtenTot < len) {
		if (writtenTot + PIPE_BUF <= len) {
			writtenNew = write(fd, str, PIPE_BUF);
		} else {
			writtenNew = write(fd, str, len - writtenTot);
		}
		if (writtenNew <= 0) {
			perror("write");
			return writtenNew;
		}
		str += writtenNew;
		writtenTot += writtenNew;
	}
	return 0;
}

char *recv_string(int fd) {
	size_t len;
	if (read(fd, &len, sizeof(size_t)) != sizeof(size_t)) {
		perror("read");
		return NULL;
	}
	char *res = malloc((len+1)*sizeof(char));
	if (res == NULL) {
		perror("malloc");
		return NULL;
	}
	char *cur = res;
	size_t nbReadTot = 0, nbReadNew;
	while (nbReadTot < len) {
		if (nbReadTot + PIPE_BUF <= len) {
			nbReadNew = read(fd, cur, PIPE_BUF);
		} else {
			nbReadNew = read(fd, cur, len - nbReadTot);
		}
		if (nbReadNew <= 0) {
			perror("read");
			return NULL;
		}
		cur += nbReadNew;
		nbReadTot += nbReadNew;
	}

	*cur = '\0';
	return res;
}

int send_argv(int fd, char *argv[]) {
	size_t count = 0;
	char **curStr = argv;
	while (*curStr != NULL) {
		++count;
		++curStr;
	}

	int st;
	if ((st = write(fd, &count, sizeof(size_t))) <= 0) {
		perror("write");
		return st;
	}

	for (size_t i=0 ; i<count ; ++i) {
		int res = send_string(fd, argv[i]);
		if (res) {
			return res;
		}
	}

	return 0;
}

char **recv_argv(int fd) {
	size_t count;
	if (read(fd, &count, sizeof(size_t)) != sizeof(size_t)) {
		perror("read");
		return NULL;
	}
	char **res = malloc((count+1)*sizeof(char*));
	char **curStr = res;
	for (size_t s=0 ; s<count ; ++s) {
		*curStr = recv_string(fd);
		++curStr;
	}
	*curStr = NULL;
	return res;
}
