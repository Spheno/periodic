#define _POSIX_C_SOURCE 200112L

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>


int main (int argc, char *argv[]) {
	// Check a command has been specified
	if (argc != 2) {
		fprintf(stderr, "You need to specify a program to launch\n");
		exit(1);
	}

	// Launch the intermediate process
	pid_t firstChild = fork();
	if (firstChild == 0) {
		// Intermediate process
		// Create a new session
		if (setsid() < 1) {
			perror("setsid");
			exit(1);
		}
		// Launch the daemon's process
		pid_t pidDaemon = fork();
		if (pidDaemon == 0) {
			// Daemon's process
			// Daemon's environment's configuration
			umask(0); // Set umask to 0 so rights aren't limited
			// Remove ways to read and display data because it has no TTY
			close(STDIN_FILENO);
			close(STDOUT_FILENO);
			close(STDERR_FILENO);
			if (argv[1][0] == '/') {
				// Absolute path
				chdir("/"); // Set working directory to root folder
				execl(argv[1], argv[1], NULL);
			} else {
				// Relative path
				// Save current directory
				char *pwd = getenv("PWD");
				if (!pwd) {
					exit(2);
				}
				// Compute paths to files
				// 2 for the '/' and the '\0'
				char *pathToDaemon = calloc(strlen(pwd) + strlen(argv[1]) + 2, sizeof(char));
				strcpy(pathToDaemon, pwd);
				strcat(pathToDaemon, "/");
				strcat(pathToDaemon, argv[1]);
				chdir("/"); // Set working directory to root folder
				execl(pathToDaemon, argv[1], NULL);
				free(pathToDaemon);
			}
			// Unable to launch the program
			perror("execl");
			exit(2);
		} else if (pidDaemon < 0) {
			// Unable to create daemon's process
			perror("fork daemon");
			exit(3);
		}
		exit(0);
	} else if (firstChild < 0) {
		// Unable to create the intermediate process
		perror("fork launcher");
		exit(3);
	}
	// Initial process
	// Wait the intermediate process
	wait(NULL);

	return 0;
}
