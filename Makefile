################################
# Makefile du projet periodic #
# Auteurs (groupe TP2A) :    #
# CABODI Alexis             #
# LAKHAL Mohamed           #
###########################

#####################
# Variables utiles #
###################

CC=gcc
CFLAGS=-Wall -g -std=c99
LDFLAGS=-g
SOCFLAGS=$(CFLAGS) -fPIC
SOLDFLAGS=$(LDFLAGS) -shared

TARGETS=libmessage.so now when launch_daemon periodic period

###########################
# Règles de construction #
#########################

all: $(TARGETS)

libmessage.so: message.o
	$(CC) $(SOLDFLAGS) -o $@ $^

message.o: message.c message.h
	$(CC) -c $(SOCFLAGS) -o $@ $<

now: now.o
	$(CC) $(LDFLAGS) $^ -o $@

now.o: now.c
	$(CC) -c $(CFLAGS) -o $@ $<

when: when.o
	$(CC) $(LDFLAGS) $^ -o $@

when.o: when.c
	$(CC) -c $(CFLAGS) -o $@ $<

launch_daemon: launch_daemon.o
	$(CC) $(LDFLAGS) $^ -o $@

launch_daemon.o: launch_daemon.c
	$(CC) -c $(CFLAGS) -o $@ $<

periodic: periodic.o libmessage.so
	$(CC) $(LDFLAGS) -o $@ $< -L. -lmessage

periodic.o: periodic.c
	$(CC) -c $(CFLAGS) -o $@ $<

period: period.o libmessage.so
	$(CC) $(LDFLAGS) -o $@ $< -L. -lmessage

period.o: period.c period.h
	$(CC) -c $(CFLAGS) -o $@ $<

##############################
# Commandes supplémentaires #
############################

clean:
	rm -f *.o

mrproper: clean
	rm -f $(TARGETS)

