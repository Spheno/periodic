#define _POSIX_C_SOURCE 200809L

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "message.h"

#define BUFSIZE 256

/**
 * Message rappelant le synopsis de la fonction en cas d'erreur
 */
void usage();

/**
 * Lit le PID du daemon dans le fichier /tmp/period.pid_t
 *
 * @return	pid_t	pid	 PID du daemon
 */
pid_t get_daemon_PID();
