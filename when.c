#include <time.h>
#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {
	if (argc != 2) {
		fprintf(stderr, "Error: 1 argument is required!\n");
		return 1;
	}

	// Amount of seconds since Epoch
	time_t nbSec = atoi(argv[1]);
	printf("Date corresponding to %d seconds since Epoch:\n %s", (int)nbSec, ctime(&nbSec));

	return 0;
}
