#include "periodic.h"

int main (int argc, char *argv[]) {
	// Daemon's PID
	pid_t pidDaemon = get_daemon_PID();

	// Pipe
	int entryPipe;

	if((entryPipe = open("/tmp/period.fifo", O_RDWR)) == -1) {
		fprintf(stderr, "Can't open pipe");
		perror("open pipe");
		exit(6);
	}

	// Checking parameters
	// No arguments: display current table
	// 1 argument: action remove
	// 2 arguments: error
	// 3 arguments or more: action add
	if (argc == 1) {
		// SIGUSR2 to daemon
		int retSIG2 = kill(pidDaemon, SIGUSR2);
		if(retSIG2 == -1) {
			perror("kill SIGUSR2");
			exit(4);
		}

		// Receiving current table
		char **cmd = recv_argv(entryPipe);
		size_t curr = 0;
		printf("Index\tNext\t\tPeriod\tCommand\n");
		while(cmd[curr] != NULL) {
			printf("%s\n", cmd[curr]);
			free(cmd[curr]);
			curr++;
		}
		free(cmd);

		return 0;
	}

	if (argc == 2) {
		// SIGUSR1 to daemon
		int retSIG1 = kill(pidDaemon, SIGUSR1);
		if(retSIG1 == -1) {
			perror("kill SIGUSR1");
			exit(4);
		}

		char *index = argv[1];
		send_string(entryPipe, "remove");
		send_string(entryPipe, index);
	}
	else if (argc == 3) {
		usage();
		exit(1);
	}
	else {
		// Start
		char start[BUFSIZE];
		char *argst = argv[1], *endptr;

		if (strcmp(argst, "now") == 0) {
			sprintf(start, "%lu", time(NULL));
		} else if (argst[0] == '+') {
			size_t startT = time(NULL) + strtol(argst, &endptr, 10);
			if (*endptr != '\0') {
				fprintf(stderr, "String detected: %s\n", endptr);
				exit(2);
			}
			sprintf(start, "%lu", startT);
		} else {
			strcpy(start, argst);
		}

		// SIGUSR1 to daemon
		int retSIG1 = kill(pidDaemon, SIGUSR1);
		if(retSIG1 == -1) {
			perror("kill SIGUSR1");
			exit(4);
		}

		send_string(entryPipe, "add");
		send_string(entryPipe, start);
		send_string(entryPipe, argv[2]);
		send_argv(entryPipe, argv+3);

	}

	return 0;
}

void usage() {
	fprintf(stderr, "usage : ./periodic start period cmd [args]\n");
}

pid_t get_daemon_PID() {
	pid_t pidDaemon;
	FILE *ficPID = fopen("/tmp/period.pid", "r");
	if (!ficPID) {
		perror("fopen ficPID");
		exit(1);
	}

	char buf[BUFSIZE];

	fgets(buf, BUFSIZE, ficPID);
	pidDaemon = atol(buf);

	int ret = fclose(ficPID);
	if(ret == EOF) {
		perror("fclose ficPID");
		exit(1);
	}

	return pidDaemon;
}
