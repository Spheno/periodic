#define _POSIX_C_SOURCE 200809L

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <stdbool.h>
#include <wait.h>

#include "message.h"

#define MAX_COMMANDS	1024

struct scheduled_command {
	time_t	start; // Next start time
	time_t	period; // Time to wait after next launch before relaunch
	char**	command; // argv for the new command (including path)
	int index; // Initial index in the table for log files
	struct scheduled_command *next; // Next command in the list
};

/////////////////////////////////////////////////
// Functions to manipulate the commands table //
///////////////////////////////////////////////

/**
 * Inserts a new command in the list
 *
 * @param	struct scheduled_command*	self	Old list of commands
 * @param	time_t	start	Time at which the command will launch
 * @param	time_t	period	Time difference between two launches of the command
 * @param	char**	command	Command to launch with its parameters (like argv)
 * @param	int	index	Potential index for the command in the list (begin with 0)
 *
 * @return	struct scheduled_command*	New list
 */
struct scheduled_command* ct_add(struct scheduled_command *self, time_t start, time_t period, int index, char** command);

/**
 * Removes a command from the list
 *
 * @param	struct scheduled_command*	self	List of commands
 * @param	size_t	index	Index of the command to remove
 *
 * @return	struct scheduled_command*	New list
 */
struct scheduled_command* ct_remove(struct scheduled_command *self, size_t index);

/**
 * Deallocates a list
 *
 * @param	struct scheduled_command*	self	List to free
 */
void ct_destroy(struct scheduled_command *self);

//////////////////////
// SIGNAL HANDLERS //
////////////////////

/**
 * Edits the command table according to the received messages
 * Triggered by SIGUSR1
 */
void sigSetCmd(int signum);

/**
 * Sends the whole commands table
 * Triggered by SIGUSR2
 */
void sigDumpTable(int signum);

/**
 * Tells the program it can end
 * Triggered by SIGINT, SIGQUIT and SIGTERM
 */
void sigEndDaemon(int signum);

/**
 * Executes wait() to destroy a zombie
 * Triggered by SIGCHLD
 */
void sigWaitChild(int signum);

/**
 * Tells the program it has to check the table to launch commands
 * Triggered by SIGALRM
 */
void sigAlarm(int signum);
