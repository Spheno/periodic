#include <time.h>
#include <stdio.h>

int main (int argc, char *argv[]) {
	printf("Elapsed seconds since Epoch:\n %d seconds\n", (int)time(NULL));

	return 0;
}
