#ifndef MESSAGE_H
#define MESSAGE_H

#define _POSIX_C_SOURCE 200809L

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>

/**
 * Sends a string into a file
 *
 * @param	int	fd	File descriptor with write access
 * @param	char*	str	C-string to write in fd
 *
 * @return	int	0 on success, last write()'s result otherwise
 */
int send_string(int fd, char *str);

/**
 * Receives a string sent with send_string() from a file
 *
 * @param	int	fd	File descriptor with read access
 *
 * @return	char*	NULL on error, received C-string on success (needs a free())
 */
char *recv_string(int fd);

/**
 * Sends an array of strings into a file
 *
 * @param	int	fd	File descriptor with write access
 * @param	char**	str	NULL-terminated array of C-strings
 *
 * @return	int	0 on success, last write()'s result otherwise
 */
int send_argv(int fd, char *argv[]);

/**
 * Receives an array of strings sent with send_argv() from a file
 *
 * @param	int	fd	File descriptor with read access
 *
 * @return	char**	NULL-terminated array of C-strings (needs free() for each string and for the array), or NULL
 */
char **recv_argv(int fd);

#endif // MESSAGE_H
